package com.redcurb.Online4Life;
import android.app.Application;
import android.util.Log;

import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseInstallation;
import com.parse.SaveCallback;

public class OFLApplication extends Application {
    static String parseInstallationId;
    static String parseId;
    @Override
    public void onCreate() {
        super.onCreate();
        // NOTE- EZER: Replace app ids if parse app changes - init parse only here
        Parse.initialize(this, "t09jUS3DF9UJGhuNv1ZccYe8Mpd1i4ya928RM4sB", "nbbVyz8gQeZ9eN5spXatWc8qVC26sekhSvpsIGXp");
        final ParseInstallation installation = ParseInstallation.getCurrentInstallation();
        installation.saveInBackground(
            new SaveCallback() {
                @Override
                public void done(ParseException arg0) {
                    parseInstallationId = installation.getInstallationId();
                    parseId = installation.getObjectId();
                    Log.d("OFLApplication", "installation Id " + parseInstallationId);
                    Log.d("OFLApplication", "parseId " + parseId);
                }
            }
        );
    }
    
    public static String getInstallationId() {
        return  parseInstallationId;
    }

    public static String getParseId() {
        return  parseId;
    }
}